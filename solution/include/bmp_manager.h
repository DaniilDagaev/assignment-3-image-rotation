//
// Created by imia on 10.01.2023.
//

#ifndef IMAGE_TRANSFORMER_BMP_MANAGER_H
#define IMAGE_TRANSFORMER_BMP_MANAGER_H


#include "rotation.h"
#include "util.h"

#define PER_METER  2000
#define TYPE  0x4D42
#define BIT_COUNT 24
#pragma pack(push, 1)

#pragma pack(pop)
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
    /* коды других ошибок  */
};
enum read_status from_bmp( FILE* file_input, struct image* img );

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum write_status to_bmp(FILE* file_out, struct image const* img );



#endif //IMAGE_TRANSFORMER_BMP_MANAGER_H
