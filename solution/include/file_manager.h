//
// Created by imia on 11.01.2023.
//

#ifndef IMAGE_TRANSFORMER_FILE_MANAGER_H
#define IMAGE_TRANSFORMER_FILE_MANAGER_H


#include "bmp_manager.h"
#include "util.h"
#include <stdbool.h>
#define OK 0
#define EXCEPTION 1
int open_file(FILE** file, char const* file_name, char const* mode);


int file_handle(char* argv[], FILE ** file_input, FILE ** file_output);
int read_file(FILE ** file_input, FILE ** file_output , struct image *img);
int write_file(FILE ** file_input, FILE ** file_output , struct image *img);
int close_files(FILE *file_first, FILE *file_second);
int close_file(FILE *file_open);
#endif //IMAGE_TRANSFORMER_FILE_MANAGER_H
