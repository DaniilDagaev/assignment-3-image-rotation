//
// Created by imia on 10.01.2023.
//

#ifndef IMAGE_TRANSFORMER_IMG_MANAGER_H
#define IMAGE_TRANSFORMER_IMG_MANAGER_H

#include <stdint.h>
#include <stdlib.h>


struct __attribute__ ((__packed__)) image  {
    uint64_t width, height;
    struct pixel* data;
};
struct __attribute__ ((__packed__)) pixel { uint8_t b, g, r; };
struct image create_img(uint64_t width, uint64_t height);
void remove_images(struct image img_first, struct image img_second);
void remove_img(struct image *img);
#endif //IMAGE_TRANSFORMER_IMG_MANAGER_H
