//
// Created by imia on 11.01.2023.
//

#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H
#include "img_manager.h"



struct image rotate_img(struct image new_img);


#endif //IMAGE_TRANSFORMER_ROTATION_H
