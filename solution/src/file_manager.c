//
// Created by imia on 11.01.2023.
//

#include "file_manager.h"


int open_file(FILE** file, char const* file_name, char const* mode){
    *file = fopen(file_name, mode);
    return *file != NULL;
}

int close_file(FILE *file_open) {
    if (!(file_open == NULL || fclose(file_open) == EOF)) {
        return true;
    }
    return false;
}
int file_handle(char* argv[], FILE ** file_input, FILE ** file_output){
    if (!open_file(file_input, argv[1], "rb")) {
        printErr( "Can't open file");
        return true;
    }

    if (!open_file(file_output, argv[2], "wb")) {
        printErr( "Can't write file");
        close_file(*file_input);
        return true;
    }
    return false;
}
int read_file(FILE ** file_input, FILE ** file_output , struct image *img){
    if (from_bmp(*file_input, img) != READ_OK) {
        printErr( "can't read file");
        close_files(*file_input,*file_output);
        return true;
    }
    return false;
}
int close_files(FILE *file_first, FILE *file_second){
    if(close_file(file_first)|| close_file(file_second)){
        return true;
    }
    return false;
}
int write_file(FILE ** file_input, FILE ** file_output , struct image *img){
    if (img->width != 0 && to_bmp(*file_output, img) != WRITE_OK) {
        printErr( "Can't write filea");
        remove_img(img);
        close_files(*file_input,*file_output);
        return true;
    }
    return false;
}
