//
// Created by imia on 11.01.2023.
//



#include "rotation.h"


struct image rotate_img(struct image new_img) {
    const uint64_t height = new_img.height;
    const uint64_t width = new_img.width;
    struct image rotate_img = create_img(height, width);
    if(!rotate_img.data){
        return rotate_img;
    }
    for (uint64_t i = 0; i < width; i++) {
        for (uint64_t j = 0; j < height; j++) {
            rotate_img.data[j + i * rotate_img.width] = new_img.data[i + (height - j - 1) * width];
        }
    }
    return rotate_img;
}
