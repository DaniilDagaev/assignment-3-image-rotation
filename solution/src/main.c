#include "util.h"

#include "file_manager.h"
#include "rotation.h"



int main(int argc, char **argv) {
	if (argc < 3 || argc > 4){
        printErr("Wrong args");
        return EXCEPTION;
    }
    FILE* file_input = NULL;
    FILE *file_output = NULL;
    struct image img = {0};
    if(file_handle(argv,&file_input, &file_output)){
        return EXCEPTION;
    }
    if(read_file(&file_input, &file_output, &img)){
        return EXCEPTION;
    }
    struct image rotated_img = rotate_img(img);
    if(write_file(&file_input,&file_output,&rotated_img)){
        return EXCEPTION;
    }
    println("Image rotated");
    if(!close_files(file_input,file_output)){
        printErr( "Can't close");
        remove_images(img, rotated_img);
        return EXCEPTION;
    }
    remove_images(img, rotated_img);
    return OK;
}
