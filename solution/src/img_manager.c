//
// Created by imia on 10.01.2023.
//


#include "img_manager.h"

struct image create_img(uint64_t width, uint64_t height){
    return (struct image){
            .width = width,
            .height = height,
            .data = (struct pixel*)malloc(sizeof(struct pixel)*width*height)
    };
}

void remove_img(struct image *img){
    free(img->data);
}
void remove_images(struct image img_first, struct image img_second){
    remove_img(&img_first);
    remove_img(&img_second);
}

