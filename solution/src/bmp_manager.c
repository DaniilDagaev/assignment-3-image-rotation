//
// Created by imia on 10.01.2023.
//

#include "bmp_manager.h"

struct __attribute__ ((__packed__)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static uint64_t calc_padding(const uint64_t img_width) {
    return (img_width % 4) == 0 ? 0 : (4 - 3 * img_width % 4);
}
static uint64_t get_img_size(const struct image *image) {
    const uint64_t width = image->width;
    const uint64_t height = image->height;
    const uint64_t padding = calc_padding(width);
    return width * height * sizeof(struct pixel) + width * height * padding;
}

static struct bmp_header create_bmp_header(struct image const* img) {
    return (struct bmp_header) {
            .biCompression = 0,
            .bfType = TYPE,
            .biClrImportant = 0,
            .biSizeImage = get_img_size(img),
            .bfReserved = 0,
            .bOffBits = 54,
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biBitCount = BIT_COUNT,
            .bfileSize = get_img_size(img) + sizeof(struct bmp_header),
            .biXPelsPerMeter = PER_METER,
            .biYPelsPerMeter = PER_METER,
            .biClrUsed = 0,
            .biPlanes = 1,
    };
}




enum write_status to_bmp( FILE* file_out, struct image const* img ) {
    const uint64_t height = img->height;
    const uint64_t width = img->width;
    struct bmp_header h = create_bmp_header(img);
    if (!fwrite(&h, sizeof(struct bmp_header), 1, file_out)) {
        return WRITE_ERROR;
    }
    for (uint64_t i = 0; i < height; i++) {
        if (!fwrite(img->data + width * i, width * sizeof (struct pixel), 1, file_out)) {
            return WRITE_ERROR;
        }
        if (!fwrite(img->data, calc_padding(width), 1, file_out)){
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
enum read_status from_bmp( FILE* file_input, struct image* img) {
    struct bmp_header header = {0};
    if(!fread(&header, sizeof( struct bmp_header ), 1, file_input )) {
        return READ_INVALID_HEADER;
    }
    if(header.bfType != TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    if(header.biBitCount != BIT_COUNT){
        return READ_INVALID_BITS;
    }
    struct image img_r = create_img(header.biWidth, header.biHeight);
    uint64_t counter = 0;
    while ( counter < header.biHeight) {
        uint64_t f_read = fread( img_r.data + counter * header.biWidth, sizeof( struct pixel), header.biWidth, file_input );
        if(f_read!=header.biWidth) {
            return READ_INVALID_BITS;
        }
        if(fseek( file_input, (long) calc_padding(header.biWidth), SEEK_CUR ) != 0) {
            return READ_INVALID_BITS;
        };
        counter++;
    }
    img->data = img_r.data;
    img->height = header.biHeight;
    img->width = header.biWidth;
    return READ_OK;
}
